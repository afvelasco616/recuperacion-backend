package com.adsi.recuperacion.repository;

import com.adsi.recuperacion.domain.Paquete;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ParquetRepository extends JpaRepository<Paquete, Long> {

    Optional<Paquete> findById (Long id);


}

package com.adsi.recuperacion.service;

import com.adsi.recuperacion.service.dto.PaqueteDTO;

import java.util.List;

public interface PaqueteService {

    public List<PaqueteDTO> getAll();

    public PaqueteDTO save( PaqueteDTO paqueteDTO);

    public PaqueteDTO update(PaqueteDTO paqueteDTO);

    public PaqueteDTO getById(long id);
}

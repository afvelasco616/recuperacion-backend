package com.adsi.recuperacion.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.crypto.Data;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter

public class PaqueteDTO implements Serializable {

    @Id
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate expeditionDate;

    @Column(length = 50)
    private String description;

    private int Weight;

    @Column(length = 30)
    private String sender_name;

    @Column(length = 30)
    private String addressee;
}

package com.adsi.recuperacion.service.dto;

import com.adsi.recuperacion.domain.Paquete;

public class PaqueteTranformer {

    public static PaqueteDTO getPaqueteDTOFromPaquete(Paquete paquete){
        if (paquete == null){
            return null;
        }

        PaqueteDTO dto = new PaqueteDTO();

        dto.setId(paquete.getId());
        dto.setExpeditionDate(paquete.getExpeditionDate());
        dto.setDescription(paquete.getDescription());
        dto.setWeight(paquete.getWeight());
        dto.setSender_name(paquete.getSender_name());
        dto.setAddressee(paquete.getAddressee());

        return dto;
    }

    public static Paquete getPaqueteFromPaqueteDTO(PaqueteDTO dto){
        if (dto == null){
            return null;
        }

        Paquete paquete = new Paquete();

        paquete.setId(dto.getId());
        paquete.setExpeditionDate(dto.getExpeditionDate());
        paquete.setDescription(dto.getDescription());
        paquete.setWeight(dto.getWeight());
        paquete.setSender_name(dto.getSender_name());
        paquete.setAddressee(dto.getAddressee());

        return paquete;

    }
}

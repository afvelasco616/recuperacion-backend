package com.adsi.recuperacion.service.imp;

import com.adsi.recuperacion.domain.Paquete;
import com.adsi.recuperacion.repository.ParquetRepository;
import com.adsi.recuperacion.service.PaqueteService;
import com.adsi.recuperacion.service.dto.PaqueteDTO;
import com.adsi.recuperacion.service.dto.PaqueteTranformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaqueteServiceImp implements PaqueteService {

    @Autowired
    private ParquetRepository parquetRepository;


    @Override
    public List<PaqueteDTO> getAll() {
        return parquetRepository.findAll().stream()
                .map(PaqueteTranformer::getPaqueteDTOFromPaquete)
                .collect(Collectors.toList());
    }

    @Override
    public PaqueteDTO save(PaqueteDTO paqueteDTO) {
        return PaqueteTranformer.getPaqueteDTOFromPaquete(parquetRepository
        .save(PaqueteTranformer.getPaqueteFromPaqueteDTO(paqueteDTO)));
    }

    @Override
    public PaqueteDTO update(PaqueteDTO paqueteDTO) {
        Paquete paquete = PaqueteTranformer.getPaqueteFromPaqueteDTO(paqueteDTO);
        return PaqueteTranformer.getPaqueteDTOFromPaquete(parquetRepository.save(paquete)) ;
    }

    @Override
    public PaqueteDTO getById(long id) {
        return PaqueteTranformer.getPaqueteDTOFromPaquete(parquetRepository.findById(id).get());
    }
}

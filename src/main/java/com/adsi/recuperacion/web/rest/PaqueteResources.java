package com.adsi.recuperacion.web.rest;


import com.adsi.recuperacion.service.PaqueteService;
import com.adsi.recuperacion.service.dto.PaqueteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class PaqueteResources {

    @Autowired
    PaqueteService service;

    @PostMapping("/paquete")
    public ResponseEntity<PaqueteDTO> create(@RequestBody PaqueteDTO paqueteDTO){
        return new ResponseEntity<>(service.save(paqueteDTO), HttpStatus.CREATED);
    }

    @GetMapping("/paquete")
    public ResponseEntity<List<PaqueteDTO>> get() {return ResponseEntity.status(200).body(service.getAll());}

    @GetMapping("/paquete/{id}")
    public ResponseEntity<PaqueteDTO> getById(@PathVariable Long id){
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    @PutMapping("/paquete")
    public PaqueteDTO update(@RequestBody PaqueteDTO paqueteDTO){
        return service.update(paqueteDTO);
    }


}
